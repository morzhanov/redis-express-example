import redis from "redis";
import { REDIS_PORT } from "../constants";

const redisClient = redis.createClient({
  port: +REDIS_PORT
});

redisClient.flushall();

export default function cache(req: Request, res: any, next: any) {
  const key = `${req.url}${req.method}`;
  redisClient.get(key, (err: any, data: any) => {
    if (err) throw err;

    if (data != null) {
      console.log("sending data from cache");
      res.send(JSON.parse(data));
    } else {
      next();
    }
  });
}
