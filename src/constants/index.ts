import dotenv from "dotenv";
dotenv.config();

export const NODE_ENV = process.env.NODE_ENV || "development";
export const PORT = 4000;
export const API_URI = "/api";
export const REDIS_PORT = process.env.REDIS_PORT;
export const SECRET = process.env.SECRET;
