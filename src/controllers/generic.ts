import { Response, Request } from "express";

import redis from "redis";
import { REDIS_PORT } from "../constants";

const redisClient = redis.createClient({
  port: +REDIS_PORT
});

redisClient.flushall();

export default {
  genericGET: async (req: Request, res: Response) => {
    const key = `${req.url}${req.method}`;
    const data = { message: "GENERIC GET" };
    redisClient.setex(key, 6, JSON.stringify(data));
    console.log(
      "sending data from controller, data cached and will be wiped after 6s"
    );
    res.status(200).json(data);
  },

  genericPOST: async (req: Request, res: Response) => {
    const key = `${req.url}${req.method}`;
    const data = { message: "GENERIC GET" };
    redisClient.setex(key, 6, JSON.stringify(data));
    console.log(
      "sending data from controller, data cached and will be wiped after 6s"
    );
    res.status(200).json(data);
  },

  genericPUT: async (req: Request, res: Response) => {
    const key = `${req.url}${req.method}`;
    const data = { message: "GENERIC GET" };
    redisClient.setex(key, 6, JSON.stringify(data));
    console.log(
      "sending data from controller, data cached and will be wiped after 6s"
    );
    res.status(200).json(data);
  },

  genericDELETE: async (req: Request, res: Response) => {
    const key = `${req.url}${req.method}`;
    const data = { message: "GENERIC DELETE" };
    redisClient.setex(key, 6, JSON.stringify(data));
    console.log(
      "sending data from controller, data cached and will be wiped after 6s"
    );
    res.status(200).json(data);
  }
};
