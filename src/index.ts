import http from "http";
import { printIp } from "./services/app-service";
import { PORT, NODE_ENV } from "./constants";
import { urlencoded } from "body-parser";
import express from "express";
import { API_URI } from "./constants";
import api from "./routers/api";
import cache from "./cache";

const signals = ["SIGINT", "SIGTERM"];
const app = express();

app.use(urlencoded({ extended: false }));
app.use(API_URI, cache, api);

const server = http.createServer(app);
server.listen(PORT);

const onListening = () => {
  const addr: any = server.address();
  console.log(`Listening on port ${addr.port}`);
  if (NODE_ENV === "development") {
    console.log(`This is testing instance.`);
    console.log(`To run production provide NODE_ENV = production.`);
  }
  printIp();
};

const onError = (err: any) => {
  if (err.syscall !== "listen") {
    throw err;
  }

  switch (err.code) {
    case "EACCES":
      console.log(`Port ${PORT} requires elevated privileges`);
      return process.exit(1);
    case "EADDRINUSE":
      console.log(`Port ${PORT} is already in use`);
      return process.exit(1);
    default:
      throw err;
  }
};

server.on("error", onError);
server.on("listening", onListening);

signals.forEach((signal: any) => {
  process.once(signal, () => {
    console.log(signal, " happened!");
  });
});
