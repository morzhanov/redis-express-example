import { Request, Response } from "express";

const errorHandler = () => {
  return (e: any, _: Request, res: Response) => {
    return res.status(e.status || 50).json({
      success: false,
      error_code: e.error_code || 500,
      error: e.message
    });
  };
};

export default errorHandler;
