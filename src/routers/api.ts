const { Router } = require("express");
import Generic from "../controllers/generic";
import ErrorHandler from "../middleware/error-handler";
import NotFound from "../middleware/not-found";

const router = new Router();

router
  .get("/generic", Generic.genericGET)
  .post("/generic", Generic.genericPOST)
  .put("/generic", Generic.genericPUT)
  .delete("/generic", Generic.genericDELETE)
  .use(NotFound("Not Found"))
  .use(ErrorHandler());

export default router;
